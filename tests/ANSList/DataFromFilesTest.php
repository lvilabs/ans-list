<?php


namespace Test\ANSList;

use ANSList\DataFromFiles;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;

class DataFromFilesTest extends TestCase
{

    public function testDirectoryDoesntExists()
    {
        $this->expectException(DirectoryNotFoundException::class);
        $data = new DataFromFiles('missing-directory');
        $data->list();
    }

    public function testListOk()
    {
        $data = new DataFromFiles(__DIR__ . '/../stub');
        $contents = file_get_contents(__DIR__ . '/../stub/2.json');
        $response = $data->list();
        $this->assertEquals(2, count($response));
        $this->assertEquals($contents, $response[2]);
    }

    public function testShowOk()
    {
        $data = new DataFromFiles(__DIR__ . '/../stub');
        $contents = file_get_contents(__DIR__ . '/../stub/2.json');
        $response = $data->show(2);
        $this->assertEquals($contents, $response);
    }

}
