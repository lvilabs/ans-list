<?php

namespace Test\ANSList;

use ANSList\ANSList;
use ANSList\DataInterface;
use ArcApi\Story;
use PHPUnit\Framework\TestCase;

class ANSListTest extends TestCase
{

    private $data;

    private $client;

    private $list;

    protected function setUp(): void
    {
        $this->data = $this->getMockBuilder(DataInterface::class)->getMock();
        $this->client = $this->getMockBuilder(Story::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->list = new ANSList($this->data, $this->client);
    }

    public function testIndexReturnFormatted()
    {
        $this->data->expects($this->once())
            ->method('list')
            ->willReturn(['{"type":"story","version":"0.10.4","headlines":{"basic":"Test"}}', '{"type":"story","version":"0.10.4"}']);
        $response = $this->list->index();
        $this->assertEquals(2, count($response));
        $this->assertEquals('Test', $response[0]['headline']);
    }

    public function testShowReturnFormatted()
    {
        $this->data->expects($this->once())
            ->method('show')
            ->with($this->equalTo(1))
            ->willReturn('{"type":"story","version":"0.10.4","headlines":{"basic":"Test"}}');
        $response = $this->list->show(1);
        $this->assertEquals('Test', $response['headline']);
    }

    public function testCreateError()
    {
        $this->data->expects($this->once())
            ->method('show')
            ->with($this->equalTo(1))
            ->willReturn('{"type":"story","version":"0.10.4","headlines":{"basic":"Test"}}');
        $this->client->expects($this->once())
            ->method('create')
            ->willThrowException(new \Exception());
        $this->assertFalse($this->list->exportToArc(1));
    }

    public function testCreateOk()
    {
        $this->data->expects($this->once())
            ->method('show')
            ->with($this->equalTo(1))
            ->willReturn('{"type":"story","version":"0.10.4","headlines":{"basic":"Test"}}');
        $this->client->expects($this->once())
            ->method('create')
            ->willReturn('{"type":"story","version":"0.10.4","_id":"1","headlines":{"basic":"Test"}}');
        $this->assertTrue($this->list->exportToArc(1));
    }

}
