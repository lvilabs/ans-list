<?php

require __DIR__ . '/../vendor/autoload.php';

use ANSList\ANSList;
use ANSList\DataFromFiles;
use ArcApi\Story;

$test = new ANSList(
    new DataFromFiles(__DIR__ . '/source'),
    new Story([
        'sandbox' => true,
        'org_name' => 'los-andes',
        'auth' => ['', ''],
    ])
);

var_dump($test->index());

var_dump($test->show('1'));

var_dump($test->exportToArc('1'));
