<?php

namespace ANSList;

/**
 * Interface DataInterface
 *
 * @package ANSList
 */
interface DataInterface
{

    /**
     * List all
     *
     * @return array
     */
    public function list(): array;

    /**
     * Show by ID
     *
     * @param $id
     *
     * @return string
     */
    public function show($id): string;

}
