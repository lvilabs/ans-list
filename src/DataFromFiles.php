<?php

namespace ANSList;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class DataFromFiles
 *
 * @package ANSList
 */
class DataFromFiles implements DataInterface
{

    /**
     * @var string $directory
     */
    private $directory;

    /**
     * @var Finder $finder
     */
    private $finder;

    /**
     * DataFromFiles constructor.
     *
     * @param string $directory
     */
    public function __construct(string $directory)
    {
        $this->finder = new Finder();
        $this->directory = $directory;
    }

    /**
     * {@inheritDoc}
     */
    public function list(): array
    {
        return $this->getData();
    }

    /**
     * {@inheritDoc}
     */
    public function show($id): string
    {
        return $this->getData($id);
    }

    /**
     * Get data from files
     *
     * @param null $id
     *
     * @return array|mixed
     */
    protected function getData($id = null)
    {
        $files = $this->finder
            ->in($this->directory)
            ->sortByName(true)
            ->reverseSorting()
            ->files();

        $pattern = !empty($id) ? $id . '.json' : '*.json';

        $dataFiles = [];

        foreach ($files->name($pattern) as $file) {
            /**
             * @var SplFileInfo $file
             */
            $dataFiles[$file->getFilenameWithoutExtension()] =
                file_get_contents($file->getPathName());
        }

        return !empty($id) && !empty($dataFiles) ? $dataFiles[$id] : $dataFiles;
    }

}
