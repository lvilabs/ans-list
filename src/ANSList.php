<?php

namespace ANSList;

use ANSSchema\Story;
use ArcApi\Story as HttpStory;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception;
use Swaggest\JsonSchema\InvalidValue;

/**
 * Class ANSList
 *
 * @package ANSList
 */
class ANSList
{

    /**
     * @var DataInterface $data
     */
    private $data;

    /**
     * @var HttpStory $client
     */
    private $client;

    /**
     * ANSList constructor.
     *
     * @param DataInterface $data
     * @param HttpStory $client
     */
    public function __construct(DataInterface $data, HttpStory $client)
    {
        $this->data = $data;
        $this->client = $client;
    }

    /**
     * List stories
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidValue
     */
    public function index()
    {
        $stories = [];

        foreach ($this->data->list() as $key => $item) {
            $story = Story::import(
                json_decode($item),
                (new Context())->setSkipValidation()
            );
            $stories[$key] = $this->formatStory($story);
        }

        return $stories;
    }

    /**
     * Show a story
     *
     * @param $id
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidValue
     */
    public function show($id)
    {
        $story = Story::import(
            json_decode($this->data->show($id)),
            (new Context())->setSkipValidation()
        );
        return $this->formatStory($story);
    }

    /**
     * Export to ARC
     *
     * @param $id
     *
     * @return bool
     */
    public function exportToArc($id)
    {
        $story = json_decode($this->data->show($id));

        try {
            $this->client->create($story);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Format story
     *
     * @param Story $story
     *
     * @return array
     */
    private function formatStory(Story $story)
    {
        return [
            'headline' => $story->headlines->basic ?? '',
            'subheadline' => $story->subheadlines->basic ?? '',
            'author' => [
                'name' => $story->credits->by[0]->name ?? '',
                'email' => $story->credits->by[0]->email ?? ''
            ],
            'content_elements' => array_map(function ($element) {
                return [
                    'type' => $element->type ?? '',
                    'content' => $element->content ?? ''
                ];
            }, $story->contentElements ?? []),
            'image' => $story->promoItems->basic->url ?? ''
        ];
    }

}
